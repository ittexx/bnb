<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = ['from', 'to'];

    public function bookable()
    {
        return $this->belongsTo(Bookable::class);
    }


    public function scopeBetweenDates(Builder $query, $from, $to){
        return $query->where('to', '>=', $from)
            ->where('from', '<=', $to);
    }


    public function review()
    {
        return $this->hasOne(Review::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }


    /**
     * Searching for Booking by review_key
     *
     * @return Booking|null
     */
    public static function findByReviewKey(string $reviewKey): ?Booking
    {
        return static::where('review_key', $reviewKey)->with('bookable')->get()->first();
    }


    /**
     * Add a review_key when a new Booking is created
     *
     * @return null
     */
    protected static function boot(){
        parent::boot();

        static::creating(function ($booking){
            $booking->review_key = Str::uuid();
        });
    }
}
