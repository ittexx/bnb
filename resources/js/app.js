require('./bootstrap');

import VueRouter from 'vue-router';
import router from './routes';
import Vuex from 'vuex';
import storeDefinition from './store';
import Index from './Index';
import Vue from 'vue';


window.Vue = require('vue').default;

Vue.use(VueRouter);

// Setting up Vuex
Vue.use(Vuex);
const store = new Vuex.Store(storeDefinition);


// Moment.js library
import moment from "moment";
Vue.filter("fromNow", value => moment(value).fromNow());

// Font Awesome Icons
import './font-awesom';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
Vue.component('font-awesome-icon', FontAwesomeIcon);



// Global components
import StarRating from './components/shared/components/StarRating';
Vue.component('star-rating', StarRating);

import FatalError from './components/shared/components/FatalError';
Vue.component('fatal-error', FatalError);

import ValidationErrors from './components/shared/components/ValidationErrors';
Vue.component('v-errors', ValidationErrors);

import Success from './components/shared/components/Success';
Vue.component('success', Success);

import Loading from './components/shared/components/Loading';
Vue.component('loading', Loading);

window.axios.interceptors.response.use(
    // response => {
    //     return response;
    // },
    response => response,
    error => {
        if(401 == error.response.status){
            store.dispatch("logout");
        }

        return Promise.reject(error);
    }
);

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        "index": Index
    },
    async beforeCreate(){
        this.$store.dispatch('loadStoredState');
        this.$store.dispatch('loadUser');
    }
});
