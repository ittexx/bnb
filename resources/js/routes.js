import VueRouter from 'vue-router';
import Bookables from "./components/bookables/Bookables";
import Bookable from "./components/bookable/Bookable";
import Review from "./components/review/Review";
import Basket from "./components/basket/Basket";
import Login from "./components/auth/Login";


const routes = [
    {
        path: "/",
        component: Bookables,
        name: "home"
    },
    {
        path: "/bookables/:id",
        component: Bookable,
        name: "bookable"
    },
    {
        path: "/review/:id",
        component: Review,
        name: "review"
    },
    {
        path: "/basket",
        component: Basket,
        name: "basket"
    },
    {
        path: "/auth/login",
        component: require('./components/auth/Login').default,
        name: "login"
    },
    {
        path: "/auth/register",
        component: require('./components/auth/Register').default,
        name: "register"
    }
]


const router = new VueRouter({
    routes,
    mode: 'history'
})

export default router;