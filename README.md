<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## About Project

[Udemy course](https://www.udemy.com/course/master-laravel-6-with-vuejs-fullstack-development/learn/lecture/29550240#overview) for Laravel 8 + Vue 2:

- Section 1: Project setting up
- Section 2: Laravel API & Vue routing and basics
- Section 3: Diving into Vue.js
- Section 4: Database, Models, Migrations, Seeders, Queries, API testing, HTTP requests
- Section 5: Controllers, Http Resourses
- Section 6: Forms, Events, Eloquent relations, Validation, Query Scopes, Handling errors
- Section 7: ReviewList Component
- Section 8: Review Page
- Section 9: Vuex - managing global state
- Section 10: Price Breakdown Component
- Section 11: Basket and Checkout
- Section 12: Authentication using Laravel Sanctum